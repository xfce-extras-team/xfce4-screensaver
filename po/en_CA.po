# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Xfce
# This file is distributed under the same license as the xfce-apps.xfce4-screensaver package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Steve Harris <steve@jbs.ca>, 2024
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: xfce-apps.xfce4-screensaver\n"
"Report-Msgid-Bugs-To: https://gitlab.xfce.org/\n"
"POT-Creation-Date: 2024-12-19 12:50+0100\n"
"PO-Revision-Date: 2018-10-16 16:27+0000\n"
"Last-Translator: Steve Harris <steve@jbs.ca>, 2024\n"
"Language-Team: English (Canada) (https://app.transifex.com/xfce/teams/16840/en_CA/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en_CA\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: data/xfce4-screensaver-preferences.desktop.in:3
#: src/xfce4-screensaver.desktop.in.in:4
msgid "Xfce Screensaver"
msgstr "Xfce Screensaver"

#: data/xfce4-screensaver-preferences.desktop.in:4
msgid "Set your screensaver preferences"
msgstr "Set your screensaver preferences"

#: data/xfce4-screensaver-preferences.desktop.in:5
#: src/xfce4-screensaver-preferences.ui:878
#: src/xfce4-screensaver.desktop.in.in:6
msgid "Screensaver"
msgstr "Screensaver"

#: data/xfce4-screensaver-preferences.desktop.in:11
msgid "settings;preferences;screensaver;lock screen;"
msgstr ""

#: data/xfce4-screensaver.desktop.in:4
msgid "Screensavers"
msgstr "Screensavers"

#: data/xfce4-screensaver.desktop.in:5
msgid "Screensaver themes"
msgstr "Screensaver themes"

#: savers/floaters.c:89
msgid "Show paths that images follow"
msgstr "Show paths that images follow"

#: savers/floaters.c:94
msgid "Occasionally rotate images as they move"
msgstr "Occasionally rotate images as they move"

#: savers/floaters.c:99
msgid "Print out frame rate and other statistics"
msgstr "Print out frame rate and other statistics"

#: savers/floaters.c:104
msgid "The maximum number of images to keep on screen"
msgstr "The maximum number of images to keep on screen"

#: savers/floaters.c:104
msgid "MAX_IMAGES"
msgstr "MAX_IMAGES"

#: savers/floaters.c:109
msgid "The initial size and position of window"
msgstr "The initial size and position of window"

#: savers/floaters.c:109
msgid "WIDTHxHEIGHT+X+Y"
msgstr "WIDTHxHEIGHT+X+Y"

#: savers/floaters.c:114
msgid "The source image to use"
msgstr "The source image to use"

#. translators: the word "image" here
#. * represents a command line argument
#: savers/floaters.c:1129
msgid "image - floats images around the screen"
msgstr "image - floats images around the screen"

#: savers/floaters.c:1133 savers/popsquares.c:47
#, c-format
msgid "%s. See --help for usage information.\n"
msgstr "%s. See --help for usage information.\n"

#: savers/floaters.c:1140
msgid "Failed to initialize the windowing system."
msgstr "Failed to initialize the windowing system."

#: savers/floaters.c:1146
msgid "You must specify one image.  See --help for usage information.\n"
msgstr "You must specify one image.  See --help for usage information.\n"

#: savers/slideshow.c:54
msgid "Location to get images from"
msgstr "Location to get images from"

#: savers/slideshow.c:54
msgid "PATH"
msgstr "PATH"

#: savers/slideshow.c:58
msgid "Color to use for images background"
msgstr "Colour to use for images background"

#: savers/slideshow.c:58
msgid "\"#rrggbb\""
msgstr "\"#rrggbb\""

#: savers/slideshow.c:62
msgid "Do not randomize pictures from location"
msgstr "Do not randomize pictures from location"

#: savers/slideshow.c:66
msgid "Do not try to stretch images on screen"
msgstr "Do not try to stretch images on screen"

#: savers/slideshow.c:70
msgid "Do not crop images to the screen size"
msgstr ""

#: savers/xfce-floaters.desktop.in.in:3
msgid "Floating Xfce"
msgstr "Floating Xfce"

#: savers/xfce-floaters.desktop.in.in:4
msgid "Bubbles the Xfce logo around the screen"
msgstr "Bubbles the Xfce logo around the screen"

#: savers/xfce-personal-slideshow.desktop.in.in:3
msgid "Slideshow"
msgstr "Slideshow"

#: savers/xfce-personal-slideshow.desktop.in.in:4
msgid "Display a slideshow from your Pictures folder"
msgstr "Display a slideshow from your Pictures folder"

#: savers/xfce-popsquares.desktop.in.in:3
msgid "Pop art squares"
msgstr "Pop art squares"

#: savers/xfce-popsquares.desktop.in.in:4
msgid "A pop-art-ish grid of pulsing colors."
msgstr "A pop-art-ish grid of pulsing colours."

#: src/gs-auth-pam.c:377
#, c-format
msgid "Unable to establish service %s: %s\n"
msgstr "Unable to establish service %s: %s\n"

#: src/gs-auth-pam.c:403
#, c-format
msgid "Can't set PAM_TTY=%s"
msgstr "Can't set PAM_TTY=%s"

#: src/gs-auth-pam.c:434
msgid "Incorrect password."
msgstr "Incorrect password."

#: src/gs-auth-pam.c:436 src/xfce4-screensaver-dialog.c:254
msgid "Authentication failed."
msgstr "Authentication failed."

#: src/gs-auth-pam.c:450
msgid "Not permitted to gain access at this time."
msgstr "Not permitted to gain access at this time."

#: src/gs-auth-pam.c:456
msgid "No longer permitted to access the system."
msgstr "No longer permitted to access the system."

#: src/gs-auth-pam.c:664
msgid "Username:"
msgstr "Username:"

#: src/gs-listener-dbus.c:1971
msgid "failed to register with the message bus"
msgstr "failed to register with the message bus"

#: src/gs-listener-dbus.c:1981
msgid "not connected to the message bus"
msgstr "not connected to the message bus"

#: src/gs-listener-dbus.c:1990 src/gs-listener-dbus.c:2020
msgid "screensaver already running in this session"
msgstr "screensaver already running in this session"

#: src/gs-lock-plug.c:303
msgctxt "Date"
msgid "%A, %B %e   %H:%M"
msgstr "%A, %B %e   %H:%M"

#: src/gs-lock-plug.c:394
msgid "Time has expired."
msgstr "Time has expired."

#: src/xfce4-screensaver-command.c:56
msgid "Causes the screensaver to exit gracefully"
msgstr "Causes the screensaver to exit gracefully"

#: src/xfce4-screensaver-command.c:60
msgid "Query the state of the screensaver"
msgstr "Query the state of the screensaver"

#: src/xfce4-screensaver-command.c:64
msgid "Query the length of time the screensaver has been active"
msgstr "Query the length of time the screensaver has been active"

#: src/xfce4-screensaver-command.c:68
msgid "Tells the running screensaver process to lock the screen immediately"
msgstr "Tells the running screensaver process to lock the screen immediately"

#: src/xfce4-screensaver-command.c:72
msgid "If the screensaver is active then switch to another graphics demo"
msgstr "If the screensaver is active then switch to another graphics demo"

#: src/xfce4-screensaver-command.c:76
msgid "Turn the screensaver on (blank the screen)"
msgstr "Turn the screensaver on (blank the screen)"

#: src/xfce4-screensaver-command.c:80
msgid "If the screensaver is active then deactivate it (un-blank the screen)"
msgstr "If the screensaver is active then deactivate it (un-blank the screen)"

#: src/xfce4-screensaver-command.c:84
msgid "Poke the running screensaver to simulate user activity"
msgstr "Poke the running screensaver to simulate user activity"

#: src/xfce4-screensaver-command.c:88
msgid ""
"Inhibit the screensaver from activating.  Command blocks while inhibit is "
"active."
msgstr ""
"Inhibit the screensaver from activating.  Command blocks while inhibit is "
"active."

#: src/xfce4-screensaver-command.c:92
msgid "The calling application that is inhibiting the screensaver"
msgstr "The calling application that is inhibiting the screensaver"

#: src/xfce4-screensaver-command.c:96
msgid "The reason for inhibiting the screensaver"
msgstr "The reason for inhibiting the screensaver"

#: src/xfce4-screensaver-command.c:100 src/xfce4-screensaver-dialog.c:65
#: src/xfce4-screensaver.c:57
msgid "Version of this application"
msgstr "Version of this application"

#: src/xfce4-screensaver-command.c:209
#, c-format
msgid "The screensaver is %s\n"
msgstr "The screensaver is %s\n"

#: src/xfce4-screensaver-command.c:209
msgid "active"
msgstr "active"

#: src/xfce4-screensaver-command.c:209
msgid "inactive"
msgstr "inactive"

#: src/xfce4-screensaver-command.c:220
msgid "The screensaver is not inhibited\n"
msgstr "The screensaver is not inhibited\n"

#: src/xfce4-screensaver-command.c:222
msgid "The screensaver is being inhibited by:\n"
msgstr "The screensaver is being inhibited by:\n"

#: src/xfce4-screensaver-command.c:238
#, c-format
msgid "The screensaver has been active for %d seconds.\n"
msgstr "The screensaver has been active for %d seconds.\n"

#: src/xfce4-screensaver-configure.py:151
#: src/xfce4-screensaver-preferences.c:710
msgid "Blank screen"
msgstr "Blank screen"

#: src/xfce4-screensaver-configure.py:152
msgid ""
"Powered by Display Power Management Signaling (DPMS),\n"
"Xfce Screensaver can automatically suspend your displays\n"
"to save power.\n"
"\n"
"Xfce Power Manager and other applications also manage\n"
"DPMS settings. If the settings here won't work, make sure\n"
"display power management wasn't disabled there.\n"
"If your displays are powering off at different\n"
"intervals, be sure to check for conflicting settings."
msgstr ""
"Powered by Display Power Management Signaling (DPMS),\n"
"Xfce Screensaver can automatically suspend your displays\n"
"to save power.\n"
"\n"
"Xfce Power Manager and other applications also manage\n"
"DPMS settings. If the settings here won't work, make sure\n"
"display power management wasn't disabled there.\n"
"If your displays are powering off at different\n"
"intervals, be sure to check for conflicting settings."

#: src/xfce4-screensaver-configure.py:188
msgid "Part of Xfce Screensaver"
msgstr "Part of Xfce Screensaver"

#: src/xfce4-screensaver-configure.py:199
msgid "After blanking, put display to sleep after:"
msgstr "After blanking, put display to sleep after:"

#: src/xfce4-screensaver-configure.py:201
#: src/xfce4-screensaver-configure.py:205
msgid "Never"
msgstr "Never"

#: src/xfce4-screensaver-configure.py:201
msgid "60 seconds"
msgstr "60 seconds"

#: src/xfce4-screensaver-configure.py:203
msgid "After sleeping, switch display off after:"
msgstr "After sleeping, switch display off after:"

#: src/xfce4-screensaver-configure.py:205
msgid "60 minutes"
msgstr "60 minutes"

#: src/xfce4-screensaver-configure.py:208
msgid "Max number of images"
msgstr "Max number of images"

#: src/xfce4-screensaver-configure.py:211
msgid "Show paths"
msgstr "Show paths"

#: src/xfce4-screensaver-configure.py:213
msgid "Do rotations"
msgstr "Do rotations"

#: src/xfce4-screensaver-configure.py:215
msgid "Print stats"
msgstr "Print stats"

#: src/xfce4-screensaver-configure.py:218
msgid "Location"
msgstr "Location"

#: src/xfce4-screensaver-configure.py:221
msgid "Background color"
msgstr "Background colour"

#: src/xfce4-screensaver-configure.py:223
msgid "Do not randomize images"
msgstr "Do not randomize images"

#: src/xfce4-screensaver-configure.py:225
msgid "Do not stretch images"
msgstr "Do not stretch images"

#: src/xfce4-screensaver-configure.py:227
msgid "Do not crop images"
msgstr ""

#: src/xfce4-screensaver-configure.py:419
msgid "Restore Defaults"
msgstr "Restore Defaults"

#: src/xfce4-screensaver-configure.py:432
msgid "Preferences"
msgstr "Preferences"

#: src/xfce4-screensaver-configure.py:438
msgid "About"
msgstr "About"

#: src/xfce4-screensaver-configure.py:451
msgid "Video"
msgstr "Video"

#: src/xfce4-screensaver-configure.py:762
msgid "Configure an individual screensaver"
msgstr "Configure an individual screensaver"

#: src/xfce4-screensaver-configure.py:764
msgid "screensaver name to configure"
msgstr "screensaver name to configure"

#: src/xfce4-screensaver-configure.py:766
msgid "check if screensaver is configurable"
msgstr "check if screensaver is configurable"

#: src/xfce4-screensaver-configure.py:770
msgid "Unable to configure screensaver"
msgstr "Unable to configure screensaver"

#: src/xfce4-screensaver-configure.py:774
msgid "Screensaver required."
msgstr "Screensaver required."

#: src/xfce4-screensaver-configure.py:782
#, python-format
msgid "File not found: %s"
msgstr "File not found: %s"

#: src/xfce4-screensaver-configure.py:790
#, python-format
msgid "Unrecognized file type: %s"
msgstr "Unrecognized file type: %s"

#: src/xfce4-screensaver-configure.py:794
#, python-format
msgid "Failed to process file: %s"
msgstr "Failed to process file: %s"

#: src/xfce4-screensaver-configure.py:799
#, python-format
msgid "Screensaver %s is configurable."
msgstr "Screensaver %s is configurable."

#: src/xfce4-screensaver-configure.py:801
#, python-format
msgid "Screensaver %s is not configurable."
msgstr "Screensaver %s is not configurable."

#: src/xfce4-screensaver-configure.py:805
#, python-format
msgid "Screensaver %s has no configuration options."
msgstr "Screensaver %s has no configuration options."

#: src/xfce4-screensaver-dialog.c:63
msgid "Show debugging output"
msgstr "Show debugging output"

#: src/xfce4-screensaver-dialog.c:67
msgid "Show the logout button"
msgstr "Show the logout button"

#: src/xfce4-screensaver-dialog.c:69
msgid "Command to invoke from the logout button"
msgstr "Command to invoke from the logout button"

#: src/xfce4-screensaver-dialog.c:71
msgid "Show the switch user button"
msgstr "Show the switch user button"

#: src/xfce4-screensaver-dialog.c:73
msgid "Message to show in the dialog"
msgstr "Message to show in the dialog"

#: src/xfce4-screensaver-dialog.c:73 src/xfce4-screensaver-dialog.c:75
msgid "MESSAGE"
msgstr "MESSAGE"

#: src/xfce4-screensaver-dialog.c:75
msgid "Not used"
msgstr "Not used"

#: src/xfce4-screensaver-dialog.c:77
msgid "Monitor height"
msgstr "Monitor height"

#: src/xfce4-screensaver-dialog.c:79
msgid "Monitor width"
msgstr "Monitor width"

#: src/xfce4-screensaver-dialog.c:81
msgid "Monitor index"
msgstr "Monitor index"

#: src/xfce4-screensaver-dialog.c:216
msgid "Checking..."
msgstr "Checking..."

#: src/xfce4-screensaver-dialog.ui:70
msgid "%U"
msgstr "%U"

#: src/xfce4-screensaver-dialog.ui:98
msgid "Enter your password"
msgstr "Enter your password"

#: src/xfce4-screensaver-dialog.ui:211
msgid "You have the Caps Lock key on."
msgstr "You have the Caps Lock key on."

#: src/xfce4-screensaver-dialog.ui:271
msgid "_Switch User"
msgstr "_Switch User"

#: src/xfce4-screensaver-dialog.ui:287
msgid "_Log Out"
msgstr "_Log Out"

#: src/xfce4-screensaver-dialog.ui:303
msgid "_Cancel"
msgstr "_Cancel"

#: src/xfce4-screensaver-dialog.ui:319
msgid "_Unlock"
msgstr "_Unlock"

#: src/xfce4-screensaver-dialog.ui:377
msgid "<b>%h</b>"
msgstr "<b>%h</b>"

#: src/xfce4-screensaver-dialog.ui:391
#, c-format
msgid "<b>%s</b>"
msgstr "<b>%s</b>"

#: src/xfce4-screensaver-preferences.c:80
msgid "Settings manager socket"
msgstr "Settings manager socket"

#: src/xfce4-screensaver-preferences.c:80
msgid "SOCKET ID"
msgstr "SOCKET ID"

#: src/xfce4-screensaver-preferences.c:716
msgid "Random"
msgstr "Random"

#: src/xfce4-screensaver-preferences.c:1739
msgid "Setting locked by administrator."
msgstr "Setting locked by administrator."

#: src/xfce4-screensaver-preferences.c:1780
msgid "Could not load the main interface"
msgstr "Could not load the main interface"

#: src/xfce4-screensaver-preferences.c:1782
msgid "Please make sure that the screensaver is properly installed"
msgstr "Please make sure that the screensaver is properly installed"

#: src/xfce4-screensaver-preferences.ui:38
msgid "Screensaver Preview"
msgstr "Screensaver Preview"

#: src/xfce4-screensaver-preferences.ui:78
msgid "<b>Screensaver preview</b>"
msgstr "<b>Screensaver preview</b>"

#: src/xfce4-screensaver-preferences.ui:228
msgid "Screensaver Preferences"
msgstr "Screensaver Preferences"

#: src/xfce4-screensaver-preferences.ui:263
msgid "Power _Management"
msgstr "Power _Management"

#: src/xfce4-screensaver-preferences.ui:280
#: src/xfce4-screensaver-preferences.ui:609
msgid "Preview"
msgstr "Preview"

#: src/xfce4-screensaver-preferences.ui:371
msgid "Running as root"
msgstr "Running as root"

#: src/xfce4-screensaver-preferences.ui:387
msgid "The screen will not be locked for the root user."
msgstr "The screen will not be locked for the root user."

#: src/xfce4-screensaver-preferences.ui:428
msgid "Resolve"
msgstr "Resolve"

#: src/xfce4-screensaver-preferences.ui:474
msgid "Xfce Power Manager is not configured to handle laptop lid events"
msgstr "Xfce Power Manager is not configured to handle laptop lid events"

#: src/xfce4-screensaver-preferences.ui:490
msgid "Your computer may not be locked when you close the lid."
msgstr "Your computer may not be locked when you close the lid."

#: src/xfce4-screensaver-preferences.ui:539
msgid "Enable Screensaver"
msgstr "Enable Screensaver"

#: src/xfce4-screensaver-preferences.ui:593
msgid "Theme"
msgstr "Theme"

#: src/xfce4-screensaver-preferences.ui:691
msgid "Configure screensaver"
msgstr "Configure screensaver"

#: src/xfce4-screensaver-preferences.ui:692
msgid "Configure..."
msgstr "Configure..."

#: src/xfce4-screensaver-preferences.ui:732
msgid "Change theme after:"
msgstr "Change theme after:"

#: src/xfce4-screensaver-preferences.ui:755
#: src/xfce4-screensaver-preferences.ui:825
#: src/xfce4-screensaver-preferences.ui:996
#: src/xfce4-screensaver-preferences.ui:1173
msgid "minutes"
msgstr "minutes"

#: src/xfce4-screensaver-preferences.ui:770
msgid "Activate screensaver when computer is idle"
msgstr "Activate screensaver when computer is idle"

#: src/xfce4-screensaver-preferences.ui:801
msgid "Regard the computer as _idle after:"
msgstr "Regard the computer as _idle after:"

#: src/xfce4-screensaver-preferences.ui:840
msgid "Inhibit screensaver for fullscreen applications"
msgstr "Inhibit screensaver for fullscreen applications"

#: src/xfce4-screensaver-preferences.ui:899
msgid "Enable Lock Screen"
msgstr "Enable Lock Screen"

#: src/xfce4-screensaver-preferences.ui:943
msgid "Lock Screen with Screensaver"
msgstr "Lock Screen with Screensaver"

#: src/xfce4-screensaver-preferences.ui:973
msgid "Lock the screen after the screensaver is active for:"
msgstr "Lock the screen after the screensaver is active for:"

#: src/xfce4-screensaver-preferences.ui:1011
msgid "On Screen Keyboard"
msgstr "On Screen Keyboard"

#: src/xfce4-screensaver-preferences.ui:1041
msgid "On screen keyboard command:"
msgstr "On screen keyboard command:"

#: src/xfce4-screensaver-preferences.ui:1066
msgid "Session Status Messages"
msgstr "Session Status Messages"

#: src/xfce4-screensaver-preferences.ui:1096
msgid "Logout"
msgstr "Logout"

#: src/xfce4-screensaver-preferences.ui:1126
msgid "Logout command:"
msgstr "Logout command:"

#: src/xfce4-screensaver-preferences.ui:1150
msgid "Enable logout after:"
msgstr "Enable logout after:"

#: src/xfce4-screensaver-preferences.ui:1188
msgid "User Switching"
msgstr "User Switching"

#: src/xfce4-screensaver-preferences.ui:1218
msgid "Lock Screen with System Sleep"
msgstr "Lock Screen with System Sleep"

#: src/xfce4-screensaver-preferences.ui:1259
msgid "Lock Screen"
msgstr "Lock Screen"

#: src/xfce4-screensaver.c:58
msgid "Enable debugging code"
msgstr "Enable debugging code"

#: src/xfce4-screensaver.desktop.in.in:5
msgid "Launch screensaver and locker program"
msgstr "Launch screensaver and locker program"

#: src/xfcekbd-indicator.c:406
msgid "XKB initialization error"
msgstr "XKB initialization error"
